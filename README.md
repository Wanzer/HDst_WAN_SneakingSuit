# HDst_WAN_SneakingSuit

A derivative of HalfBakedCake's Corporate Armor.
https://codeberg.org/TwelveEyes/hd_uacarmour/

Loadout codes are `aws` for to start worn and `ars` for spares.

In contrast to Corporate Armor and Garrison Armor, it :
- Only protects as much as garrison, but keeps optimal protection for longer
- Does not provide hazmat suit resistances, but still insulates slightly against fire
- Restricts movements even less than garrison armor, and weighs less
- Does not self-heal over time
- Slows blood loss by auto bandaging wounds
- Spawns occasionally on Garrison Armor spawns