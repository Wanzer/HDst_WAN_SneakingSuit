version "4.6"

#include "zscript/sneakingsuit.zs"

class SneakingSuitHandler : EventHandler
{
    override void CheckReplacement(ReplaceEvent e)
    {
        if(!e.Replacement)
        {
            return;
        }

        switch(e.Replacement.GetClassName())
        {
            /*case 'BattleArmour':
                if(!random(0,3))
                {
                    e.Replacement = "SneakingSuit";
                }
                break;*/
            case 'GarrisonArmour':
                if(!random(0,2))
                {
                    e.Replacement = "SneakingSuit";
                }
                break;
        }
    }
}